import java.util.Scanner;

public class Hamurabi
{
    // This function prints the initial banner message to the player
    private static void displayGreeting()
    {
        System.out.println( "HAMURABI -" );
        System.out.println( "WHERE YOU GOVERN THE ANCIENT KINGDOM OF SUMERIA" );
        System.out.println( "THE OBJECT IS TO KEEP THE KINGDOM GROWING." );
        System.out.println( "(IF YOU WANT TO QUIT, SELL ALL YOUR LAND)" );
        System.out.println();
        System.out.println( "PRESS  ENTER  TO BEGIN YOUR REIGN" );
    }
    
    // This function displays the stats for this year
    private static void
        displayStats( int year, int numStarved, int numImmigrants,
                boolean plagueOccurred, int population,
                int bushelsHarvested, int yield, int bushelsAteByRats,
                int bushelsInStorage, int numAcres, int landPrice )
    {
        System.out.println();
        System.out.println( "HAMURABI, I BEG TO REPORT THAT IN YEAR " + year );
        
        if ( numStarved == 1 )
        {
            System.out.print( " 1 PERSON" );
        }
        else
        {
            System.out.print( " " + numStarved + " PEOPLE" );
        }
        System.out.println( " STARVED, AND" );
        
        if ( numImmigrants == 1 )
        {
            System.out.print( " 1 PERSON" );
        }
        else
        {
            System.out.print( " " + numImmigrants + " PEOPLE" );
        }
        System.out.println( " CAME TO THE CITY." );
        
        if ( plagueOccurred )
        {
            System.out.println( " THE PLAGUE KILLED HALF THE PEOPLE." );
        }
        
        System.out.println( " THE POPULATION IS NOW " + population + "." );
        System.out.println( " WE HARVESTED " + bushelsHarvested +
                " BUSHELS AT " + yield + " BUSHELS PER ACRE." );
        System.out.println( " RATS DESTROYED " + bushelsAteByRats +
                " BUSHELS, LEAVING " + bushelsInStorage +
                " BUSHELS IN STORAGE." );
        System.out.println( " THE CITY OWNS " + numAcres + " ACRES OF LAND." );
        System.out.println( " LAND IS WORTH " + landPrice +
                " BUSHELS PER ACRE." );
        System.out.println( "HAMURABI . . ." );
    }
    
    // This function calculates and returns this year's land price.
    // It is a random number between 17 and 22
    private static int calculateLandPrice()
    {
        int landPrice = (int) ((Math.random() * 6) + 17);
        return landPrice;
    }
    
    // This function asks the user for a numeric integer input.
    // The first parameter is a Scanner that will provide the input.
    // The second parameter is the prompt to be displayed.
    // The third parameter is the maximum value that can be input.
    // The last three parameters are used for error messages:
    //   population is the current population, numAcres is the number
    //   of acres owned by the city, and bushelsInStorage is the number
    //   of bushels in storehouses.
    // If a negative value is entered, it is converted to a positive value
    // before checking the value.  If the value is greater than the maximum
    // value that can be entered, a warning message is given, and the user
    // is reprompted for input.
    private static int askInput( Scanner inputScanner, String prompt,
            int maxValue, int population, int numAcres, int bushelsInStorage )
    {
        // The value that is input by the user
        int inputValue = 0;

        // Indicates if we've received valid numeric input
        boolean hadValidInput = false;

        // Loop until the value we've received is numeric and
        // within the allowable range
        while ( !hadValidInput )
        {
            // Prompt the user
            System.out.print( "      " + prompt + " " );

            // Check to see if there is an integer in the input
            if ( inputScanner.hasNextInt() )
            {
                // Grab the input
                inputValue = inputScanner.nextInt();

                // And now clear out the input buffer
                inputScanner.nextLine();

                // Make sure the number is >= 0
                if ( inputValue < 0 )
                {
                    inputValue = -inputValue;
                }

                // Check to see if the input is in the allowable range
                if ( inputValue > maxValue )
                {
                    // Ask for new input
                    System.out.println( "-->   HAMURABI!  THINK AGAIN --" +
                            " YOU ONLY HAVE" );
                    System.out.println( "-->   " + population + " PEOPLE, " +
                            numAcres + " ACRES, AND " +
                            bushelsInStorage + " BUSHELS IN STOREHOUSES." );
                }
                else
                {
                    // We've received valid input
                    hadValidInput = true;
                }
            }
            else
            {
                // Ask the user to input a number
                System.out.println( "   !  HAMURABI, PLEASE ENTER A NUMBER!" );

                // And now clear out the input buffer
                inputScanner.nextLine();
            }
        }

        // Return the input
        return inputValue;
    }
    
    // This function prints the final score
    private static void displayFinalScore( int bushelsInStorage )
    {
        System.out.println( "YOU HAVE SOLD ALL YOUR LAND.    YOU HAD * " +
                bushelsInStorage + " * BUSHELS." );
        System.out.println( "THE GAME IS OVER, AND YOU CAN GO BACK TO " +
                "BEING YOURSELF." );
        System.out.println( "BYE" );
    }

    public static void main( String[] args )
    {
        Scanner consoleInput = new Scanner( System.in );
        
        // The city's population
        int population = 100;
        
        // Number of immigrants this year
        int numImmigrants = 5;
        
        // Number of starved citizens this year
        int numStarved = 0;
        
        // Number of bushels in storage
        int bushelsInStorage = 2800;
        
        // Number of bushels ate by rats this year
        int bushelsAteByRats = 200;
        
        // Number of bushels harvested per acre
        int yield = 3;
        
        // Total number of bushels harvested this year
        int bushelsHarvested = 3000;
        
        // Number of acres owned by the city
        int numAcres = 1000;
        
        // The year of Hamubari's reign
        int yearOfReign = 1;
        
        // Indicates whether a plague has occurred this year
        boolean plagueOccurred = false;
        
        // This year's land price, in bushels per acre
        int landPrice;
        
        // Display the greeting message to the player
        displayGreeting();
        
        // Wait for the user to press enter to begin the game
        consoleInput.nextLine();
        
        // Loop until all of the land has been sold
        while ( numAcres > 0 )
        {
            // Determine the land price
            landPrice = calculateLandPrice();
            
            // If the plague occurred, half the population died
            if ( plagueOccurred )
            {
                population /= 2;
            }
            
            // Display this year's stats
            displayStats( yearOfReign, numStarved, numImmigrants,
                          plagueOccurred, population, bushelsHarvested, yield,
                          bushelsAteByRats, bushelsInStorage,
                          numAcres, landPrice );
            
            // Determine how many acres can be bought
            int maxAcresToBuy = bushelsInStorage / landPrice;
            
            // Ask how many acres to buy
            int acresToBuy = askInput( consoleInput, "BUY HOW MANY ACRES?",
                                       maxAcresToBuy, population,
                                       numAcres, bushelsInStorage );
            
            // Indicate the number of acres being bought
            System.out.println( "   *  YOU ARE BUYING " + acresToBuy +
                    " ACRES." );

            // Calculate the new number of bushels in storage
            bushelsInStorage -= acresToBuy * landPrice;
            
            // Calculate the new number of acres
            numAcres += acresToBuy;
            
            // If we bought any land, don't ask how many to sell
            if ( acresToBuy == 0 )
            {
                // Ask how many acres to sell
                int acresToSell = askInput( consoleInput,
                        "SELL HOW MANY ACRES?", numAcres, population,
                        numAcres, bushelsInStorage );
                
                // Indicate the number of acres being sold
                System.out.println( "   *  YOU ARE SELLING " + acresToSell +
                        " ACRES." );

                // Calculate new number of acres
                numAcres -= acresToSell;
                
                // Calculate new number of bushels in storage
                bushelsInStorage += acresToSell * landPrice;
            }
            
            // If we didn't sell everything, continue with calculations
            if ( numAcres > 0 )
            {

                // Ask how many bushels to distribute as food
                int bushelsForFood = askInput( consoleInput,
                        "HOW MANY BUSHELS SHALL WE DISTRIBUTE AS FOOD?",
                        bushelsInStorage, population, numAcres,
                        bushelsInStorage );

                // Indicate the number of bushels being distributed for food
                System.out.println( "   *  YOU ARE DISTRIBUTING "
                        + bushelsForFood + " BUSHELS." );

                // Calculate the number of remaining bushels
                bushelsInStorage -= bushelsForFood;

                // Calculate the number of people who starved
                // Note: each person needs exactly 20 bushels
                // Also note: this may be a negative number, and will
                // be accounted for shortly!
                numStarved = population - ( bushelsForFood / 20 );
                
                // Calculate preliminary number of immigrants:
                // If anyone starved
                // Then
                //     No one comes to the city
                // Else
                //     Calculate the number of immigrants as  half the
                //       number who could be fed by the excess
                if ( numStarved > 0 )
                {
                    numImmigrants = 0;
                }
                else
                {
                    numImmigrants = -numStarved / 2;
                    numStarved = 0;
                }
                
                // Ask how many acres to plant
                // Note that the most one person can plant is 10 acres,
                // we use half a bushel of seed per acre, and we can't
                // plant more acres than we have.
                int maxCanPlant = Math.min( population * 10,
                                            bushelsInStorage * 2 );
                maxCanPlant = Math.min( maxCanPlant, numAcres );
                int acresToPlant = askInput( consoleInput,
                        "HOW MANY ACRES SHALL WE PLANT?",
                        maxCanPlant, population, numAcres,
                        bushelsInStorage );

                // Decrease the number of bushels in storage by the amount
                // we're going to plant
                bushelsInStorage -= acresToPlant / 2;
                
                // Now we've got all the input.  Let's do our calculations.
                // Number of bushels harvested per acre is a random integer
                //   between 2 and 6
                yield = (int) (Math.random() * 5) + 2;
                
                // Calculate the number of bushels harvested in total
                bushelsHarvested = yield * acresToPlant;
                
                // Calculate the number of bushels ate by rats
                // (bushels in storage + bushels harvested) *
                //    random number between 0 and 0.07
                // (they ate between 0 and 7% of what we've got)
                bushelsAteByRats = (int) (
                        ( bushelsInStorage + bushelsHarvested ) *
                        Math.random() * 0.07
                    );
                
                // Calculate the number of bushels in storage at the end
                // of the year
                bushelsInStorage += bushelsHarvested - bushelsAteByRats;
                
                // There is a 1 in 11 chance that the plague occurs
                plagueOccurred = ( (int) ( Math.random() * 11 ) == 0 );
                
                // Do additional calculations of how many people
                // come to the city.  The minimum number of immigrants is 0,
                // and the maximum is 50.
                numImmigrants += ( 5 - yield ) * ( bushelsInStorage / 600 ) + 1;
                numImmigrants = Math.min( numImmigrants, 50 );
                numImmigrants = Math.max( 0, numImmigrants );
                
                // Calculate the new population
                population += numImmigrants - numStarved;
                
                // Increment the year
                yearOfReign++;
            }
        }
        
        // All the land has been sold, so display the final score
        displayFinalScore( bushelsInStorage );
    }

}
